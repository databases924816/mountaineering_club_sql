CREATE SCHEMA MountaineeringClub;

CREATE TABLE MountaineeringClub.Climbers (
  Climber_ID SERIAL PRIMARY KEY,
  Name VARCHAR(100) NOT NULL CHECK (Name IS NOT NULL),
  Address VARCHAR(255) NOT NULL
);

CREATE TABLE MountaineeringClub.Mountains (
  Mountain_ID SERIAL PRIMARY KEY,
  Name VARCHAR(100) NOT NULL,
  Height DECIMAL(10, 2) NOT NULL CHECK (Height >= 0),
  Country VARCHAR(100) UNIQUE,
  Area VARCHAR(100) NOT NULL
);

CREATE TABLE MountaineeringClub.Climbs (
  Climb_ID SERIAL PRIMARY KEY,
  Start_Date DATE NOT NULL CHECK (Start_Date > DATE '2000-01-01'),
  End_Date DATE NOT NULL CHECK (End_Date > DATE '2000-01-01'),
  Climber_ID INT NOT NULL REFERENCES MountaineeringClub.Climbers(Climber_ID),
  Mountain_ID INT NOT NULL REFERENCES MountaineeringClub.Mountains(Mountain_ID)
);

CREATE TABLE MountaineeringClub.ClimbingGear (
  Gear_ID SERIAL PRIMARY KEY,
  Gear_Name VARCHAR(100) NOT NULL,
  Gear_Type VARCHAR(100)
);

CREATE TABLE MountaineeringClub.GearUsed (
  Climb_ID INT NOT NULL REFERENCES MountaineeringClub.Climbs(Climb_ID),
  Gear_ID INT NOT NULL REFERENCES MountaineeringClub.ClimbingGear(Gear_ID),
  PRIMARY KEY (Climb_ID, Gear_ID)
);

CREATE TABLE MountaineeringClub.WeatherConditions (
  Weather_ID SERIAL PRIMARY KEY,
  Weather_Type VARCHAR(100) NOT NULL,
  Description VARCHAR(255)
);

CREATE TABLE MountaineeringClub.ClimbWeather (
  Climb_ID INT NOT NULL REFERENCES MountaineeringClub.Climbs(Climb_ID),
  Weather_ID INT NOT NULL REFERENCES MountaineeringClub.WeatherConditions(Weather_ID),
  PRIMARY KEY (Climb_ID, Weather_ID)
);

CREATE TABLE MountaineeringClub.MountainRanges (
  Range_ID SERIAL PRIMARY KEY,
  Range_Name VARCHAR(100) NOT NULL,
  Country VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE MountaineeringClub.MountainInRanges (
  Mountain_ID INT NOT NULL REFERENCES MountaineeringClub.Mountains(Mountain_ID),
  Range_ID INT NOT NULL REFERENCES MountaineeringClub.MountainRanges(Range_ID),
  PRIMARY KEY (Mountain_ID, Range_ID)
);

CREATE TABLE MountaineeringClub.ClimbingRoutes (
  Route_ID SERIAL PRIMARY KEY,
  Mountain_ID INT NOT NULL REFERENCES MountaineeringClub.Mountains(Mountain_ID),
  Route_Name VARCHAR(100) NOT NULL,
  Difficulty_Level INT NOT NULL CHECK (Difficulty_Level BETWEEN 1 AND 10)
);

INSERT INTO MountaineeringClub.Climbers (Name, Address) VALUES
  ('John Doe', '123 Main St'),
  ('Jane Smith', '456 Oak Ave'),
  ('Michael Johnson', '789 Elm Blvd');

INSERT INTO MountaineeringClub.Mountains (Name, Height, Country, Area) VALUES
  ('Mount Everest', 8848.86, 'Nepal', 'Himalayas'),
  ('K2', 8611, 'Pakistan', 'Karakoram'),
  ('Denali', 6190, 'United States', 'Alaska');

INSERT INTO MountaineeringClub.Climbs (Start_Date, End_Date, Climber_ID, Mountain_ID) VALUES
  ('2023-05-10', '2023-05-20', 1, 1),
  ('2022-08-15', '2022-08-25', 2, 2),
  ('2024-01-01', '2024-01-05', 3, 3);

INSERT INTO MountaineeringClub.ClimbingGear (Gear_Name, Gear_Type) VALUES
  ('Rope', 'Safety'),
  ('Harness', 'Safety'),
  ('Ice Axe', 'Tool');

INSERT INTO MountaineeringClub.GearUsed (Climb_ID, Gear_ID) VALUES
  (1, 1),
  (1, 2),
  (2, 3);

INSERT INTO MountaineeringClub.WeatherConditions (Weather_Type, Description) VALUES
  ('Sunny', 'Clear skies'),
  ('Snowy', 'Heavy snowfall'),
  ('Cloudy', 'Overcast conditions');

INSERT INTO MountaineeringClub.ClimbWeather (Climb_ID, Weather_ID) VALUES
  (1, 1),
  (2, 2),
  (3, 3);

INSERT INTO MountaineeringClub.MountainRanges (Range_Name, Country) VALUES
  ('Himalayas', 'Nepal'),
  ('Karakoram', 'Pakistan'),
  ('Alaska Range', 'United States');

INSERT INTO MountaineeringClub.MountainInRanges (Mountain_ID, Range_ID) VALUES
  (1, 1),
  (2, 2),
  (3, 3);

INSERT INTO MountaineeringClub.ClimbingRoutes (Mountain_ID, Route_Name, Difficulty_Level) VALUES
  (1, 'South Col Route', 9),
  (2, 'Abruzzi Spur', 8),
  (3, 'West Buttress', 7);

ALTER TABLE MountaineeringClub.Climbers ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE MountaineeringClub.Mountains ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE MountaineeringClub.Climbs ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE MountaineeringClub.ClimbingGear ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE MountaineeringClub.GearUsed ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE MountaineeringClub.WeatherConditions ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE MountaineeringClub.ClimbWeather ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE MountaineeringClub.MountainRanges ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE MountaineeringClub.MountainInRanges ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE MountaineeringClub.ClimbingRoutes ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;


-- Check if the value has been set for the existing rows.
SELECT * FROM MountaineeringClub.Climbers;
SELECT * FROM MountaineeringClub.Mountains;
SELECT * FROM MountaineeringClub.Climbs;
SELECT * FROM MountaineeringClub.ClimbingGear;
SELECT * FROM MountaineeringClub.GearUsed;
SELECT * FROM MountaineeringClub.WeatherConditions;
SELECT * FROM MountaineeringClub.ClimbWeather;
SELECT * FROM MountaineeringClub.MountainRanges;
SELECT * FROM MountaineeringClub.MountainInRanges;
SELECT * FROM MountaineeringClub.ClimbingRoutes;